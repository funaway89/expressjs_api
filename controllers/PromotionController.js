'use strict'

const express = require('express');
const app     = express();
const fetch   = require('node-fetch');

const api_key = "0cbf70a1308791f29a747050d73e7ae38a68008a6b8388910c42eab5ca098bb2";
const api_secret = "d37d4e588868149bedd0bd846098dc49de26479056af4f18fe7ede67386c0d9e";

var apiHeaders = {
    "api-key": api_key,
    "api-secret": api_secret
}

module.exports = {
    get: (req, res) => {
        fetch("https://api-sandbox.ifeelgoods.com/fulfillment/v2/promotions/" + [req.params.promotionId], { method: 'GET', headers: apiHeaders })
        .then(res => res.json())
        .then(data => {
            return res.send(data);
        })
        .catch(err => {
            return false;
        });
        return false;
    },
}
