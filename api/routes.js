'use strict';
module.exports = function(app) {
  let promoController = require('../controllers/PromotionController');

  // todoList Routes
  app.route('/promotion/:promotionId')
    .get(promoController.get);
};
